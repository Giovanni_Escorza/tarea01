package com.tarea1.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tarea1.model.DetalleVenta;

public interface IDetalleVentaDAO extends JpaRepository<DetalleVenta, Integer>{

}
