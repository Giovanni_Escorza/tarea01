package com.tarea1.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tarea1.model.Persona;

public interface IPersonaDAO extends JpaRepository<Persona, Integer>{

}
