package com.tarea1.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.tarea1.model.Venta;
import com.tarea1.service.IVentaService;

@RestController
@RequestMapping("/ventas")
public class VentaController {
	@Autowired
	private IVentaService service;

	@GetMapping(produces = "application/json")
	public ResponseEntity<List<Venta>> listar() {
		List<Venta> Venta = new ArrayList<>();
		Venta = service.listar();
		return new ResponseEntity<List<Venta>>(Venta, HttpStatus.OK);
	}
	
	@PostMapping(produces = "application/json", consumes = "application/json")
	public ResponseEntity<Object> reguistrar(@RequestBody Venta Venta) {
		Venta pac = new Venta();
		pac = service.registrar(Venta);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(pac.getIdVenta()).toUri();
		return ResponseEntity.created(location).build();
	}
}
