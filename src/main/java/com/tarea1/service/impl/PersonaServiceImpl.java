package com.tarea1.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tarea1.dao.IPersonaDAO;
import com.tarea1.model.Persona;
import com.tarea1.service.IPersonaService;
@Service
public class PersonaServiceImpl implements IPersonaService{

	@Autowired
	private IPersonaDAO dao;
	@Override
	public Persona registrar(Persona t) {
		
		return dao.save(t);
	}

	@Override
	public Persona modificar(Persona t) {
		// TODO Auto-generated method stub
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.delete(id);
		
	}

	@Override
	public Persona listarId(int id) {
		// TODO Auto-generated method stub
		return dao.findOne(id);
	}

	@Override
	public List<Persona> listar() {
		
		return dao.findAll();
	}

}
