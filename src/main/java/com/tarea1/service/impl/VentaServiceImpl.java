package com.tarea1.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tarea1.dao.IVentaDAO;
import com.tarea1.model.Venta;
import com.tarea1.service.IVentaService;

@Service
public class VentaServiceImpl implements IVentaService {

	@Autowired
	private IVentaDAO dao;

	@Transactional
	@Override
	public Venta registrar(Venta venta) {
		venta.getDetalleVenta().forEach(v -> {
			v.setVenta(venta);
		});
		return dao.save(venta);
	}

	@Override
	public Venta modificar(Venta t) {
		// TODO Auto-generated method stub
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.delete(id);

	}

	@Override
	public Venta listarId(int id) {
		// TODO Auto-generated method stub
		return dao.findOne(id);
	}

	@Override
	public List<Venta> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

}
